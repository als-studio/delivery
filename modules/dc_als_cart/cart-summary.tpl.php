<?php

/**
 * @file
 * Default implementation of a line item summary template.
 *
 * Available variables:
 * - $quantity_raw: The number of items in the cart.
 * - $quantity_label: The quantity appropriate label to use for the number of
 *   items in the shopping cart; "item" or "items" by default.
 * - $quantity: A single string containing the number and label.
 * - $total_raw: The raw numeric value of the total value of items in the cart.
 * - $total_label: A text label for the total value; "Total:" by default.
 * - $total: The currency formatted total value of items in the cart.
 * - $links: A rendered links array with cart and checkout links.
 *
 * Helper variables:
 * - $view: The View the line item summary is attached to.
 *
 * @see template_preprocess()
 * @see template_process()
 */
?>
 <div class="line-item-summary">
   <div class="line-item-summary-contents">
  <?php if ($quantity_raw): ?>
    <?php print $cart_items; ?>
  <?php endif; ?>
  <?php if ($total): ?>
   на <div class="final-price"><?php print $total_formatted; ?></div> 
  <?php endif; ?>
  <?php if ($links): ?>
    <a class="checkout-button" href="/checkout">Оформить заказ</a>
   
  <?php endif; ?>
  </div>
</div>
