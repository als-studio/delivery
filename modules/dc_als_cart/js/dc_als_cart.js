(function($, Drupal) {
/*
  window.onload = function() {
    var top = $('#block-commerce-cart-cart').offset().top;
    $('html, body').animate({scrollTop: top}, 500);
  };
*/
  Drupal.ajax = Drupal.ajax || {};
  Drupal.cart_item_width = 225;
  Drupal.cart_item_height = 135;
  
  if (Drupal.ajax) {
    Drupal.ajax.prototype.commands.after_dc_als_cart_add_to_cart_form_callback = function(ajax, response, status) {
      var cart_items_len = $('[id^="views-form-commerce-cart-block-default"] div.views-row').length;
      if (cart_items_len > 0) {
		$("#block-commerce-cart-cart-empty").hide();
      }
    };
    Drupal.ajax.prototype.commands.after_dc_als_cart_cart_block_form_callback = function(ajax, response, status) {
      var cart_items = $('[id^="views-form-commerce-cart-block-default"] div.views-row');
      if (!cart_items || cart_items.length == 0) {
		$("#block-commerce-cart-cart-empty").show();
      } else {
		$("#block-commerce-cart-cart-empty").hide();
      }
    };
  }
  
  Drupal.behaviors.als_cart = {
    attach: function(context) {
	  var cart_direction = Drupal.settings.dc_als_cart.direction;
      var cart_items_len = $('[id^="views-form-commerce-cart-block-default"] div.views-row').length;
      if (cart_items_len > 0) {
		$("#block-commerce-cart-cart-empty").hide();
      }
      // Styling cart
      var cart = $('#block-commerce-cart-cart');
	  cart.addClass(cart_direction);
      cart.once('cart-block-wrapping').wrapInner('<div class="cart-block-cart-wrapper" />').prepend('<div class="cart-block-cart-image"> <div class="pers" /> ');
      if($('#block-commerce-cart-cart .content').find('div.cart-contents').length === 0) {
        cart.find('.content').once('cart-contents-wrapping').wrapInner('<div class="cart-contents" />');
      }
      cart.find('.cart-block-cart-wrapper h2').html('Ваш заказ');
      var cart_empty_block = cart.find('.cart-empty-block, .cart-empty-page');
      cart_empty_block.html('нет товаров');
      var cart_items = $('[id^="views-form-commerce-cart-block-default"] div.views-row');
	  switch (cart_direction) {
		case "horizontal":
		  Drupal.cart_item_width = cart.find('.views-row-1').width() || 222;
		  $('[id^="views-form-commerce-cart-block-default"]').css('width', (cart_items.length * Drupal.cart_item_width)+'px');
		  break;
		case "vertical":
		  cart.find('.views-row').once('cart-items-wrapping').wrapAll('<div class="cart-items-wrapper" />')
		  Drupal.cart_item_height = cart.find('.views-row-1').height() + 24 || 222;
		  $('[id^="views-form-commerce-cart-block-default"]').css('height', ((cart_items.length * Drupal.cart_item_height) + cart.find('.line-item-summary').height() + 24) +'px');
		  break;
	  }
      if (cart_items.length > Drupal.settings.dc_als_cart.itemlimit) { // initialize carousel
        Drupal.setCarousel(cart);
      } else { // destroy carousel
        Drupal.unsetCarousel(cart);        
      }
      if (cart_empty_block.length > 0) {
        cart.find('.cart-block-cart-wrapper h2').show();
        cart.addClass('block-commerce-cart-empty');
        $('body').addClass('commerce-cart-empty');
        $('#block-views-special-offers-block-1').show();
        $("body:not('.page-checkout') #block-commerce-cart-cart").unstick();
      } else {
        cart.find('.cart-block-cart-wrapper h2').hide();
        cart.removeClass('block-commerce-cart-empty');
        $('body').removeClass('commerce-cart-empty');
        $('#block-views-special-offers-block-1').hide();
        $("body:not('.page-checkout') #block-commerce-cart-cart").once('sticky').sticky({
		topSpacing: 0,
		bottomSpacing: ($('#footer').height() + 30),
	  });
      }
      
      
    }
  };

  setButtonsActiveInactive = function(cart) {
    var items_length = $('[id^="views-form-commerce-cart-block-default"] div.views-row').length;
	switch (Drupal.settings.dc_als_cart.direction) {
	  case "horizontal":
	    var cart_block = cart.find('.view-commerce-cart-block .view-content');
		if (cart_block.scrollLeft() <= 0) {
		  cart.find('.cart-carousel-prev').addClass('disabled');
		  cart.find('.cart-carousel-next').removeClass('disabled');
		} else {
		  cart.find('.cart-carousel-prev').removeClass('disabled');
		}
		if (cart_block.scrollLeft() >= (items_length-Drupal.settings.dc_als_cart.itemlimit)*Drupal.cart_item_width) {
		  cart.find('.cart-carousel-next').addClass('disabled');
		  cart.find('.cart-carousel-prev').removeClass('disabled');
		} else {
		  cart.find('.cart-carousel-next').removeClass('disabled');
		}
	    break;
	  case "vertical":
	    var cart_block = cart.find('.view-commerce-cart-block .view-content .cart-items-wrapper');
		if (cart_block.scrollTop() <= 0) {
		  cart.find('.cart-carousel-prev').addClass('disabled');
		  cart.find('.cart-carousel-next').removeClass('disabled');
		} else {
		  cart.find('.cart-carousel-prev').removeClass('disabled');
		}
		if (cart_block.scrollTop() >= (items_length-Drupal.settings.dc_als_cart.itemlimit)*Drupal.cart_item_height) {
		  cart.find('.cart-carousel-next').addClass('disabled');
		  cart.find('.cart-carousel-prev').removeClass('disabled');
		} else {
		  cart.find('.cart-carousel-next').removeClass('disabled');
		}
	    break;
	}
  };

  Drupal.setCarouselButtons = function(cart) {
    //console.log('Drupal.setCarouselButtons');
    setButtonsActiveInactive(cart);
    cart.find('.cart-carousel-next:not("disabled"), .cart-carousel-prev:not("disabled")').click(function(e) {
      e.preventDefault();
      if (e.handled !== true) {
        var dir = '-';
        if ($(this).attr('class').indexOf("next") !== -1)
          dir = '+';
        //console.log(dir);
		switch (Drupal.settings.dc_als_cart.direction) {
		  case "horizontal":
			cart.find('.view-commerce-cart-block .view-content').animate({
			  scrollLeft: dir+'='+Drupal.cart_item_width
			}, 500, function() {
			  //код по завершении анимации
			  setButtonsActiveInactive(cart);
			});
			break;
		  case "vertical":
			cart.find('.view-commerce-cart-block .view-content .cart-items-wrapper').animate({
			  scrollTop: dir+'='+Drupal.cart_item_height
			}, 500, function() {
			  //код по завершении анимации
			  setButtonsActiveInactive(cart);
			});
			break;
		}
        e.handled = true;
      }
      return false;
    });
  };

  Drupal.setCarousel = function(cart) {
	switch (Drupal.settings.dc_als_cart.direction) {
	  case "horizontal":
	    cart.find('.cart-contents').once('cart-carousel-next').append('<div class="cart-carousel-next"><a href="javascript:void();"><span>Next</span></a></div>');
	    cart.find('.cart-contents').once('cart-carousel-prev').prepend('<div class="cart-carousel-prev"><a href="javascript:void();"><span>Prev</span></a></div>');
	    $('.view-commerce-cart-block').css('width', (Drupal.settings.dc_als_cart.itemlimit * Drupal.cart_item_width)+'px');
	    break;
	  case "vertical":
	    cart.find('.cart-contents .cart-items-wrapper').parent().once('cart-carousel-next').append('<div class="cart-carousel-next"><a href="javascript:void();"><span>Next</span></a></div>');
	    cart.find('.cart-contents .cart-items-wrapper').parent().once('cart-carousel-prev').prepend('<div class="cart-carousel-prev"><a href="javascript:void();"><span>Prev</span></a></div>');
	    $('.view-commerce-cart-block .view-content').css({
		  'height': ((Drupal.settings.dc_als_cart.itemlimit * Drupal.cart_item_height) + cart.find('.line-item-summary').height() + 24 + 30)+'px',
		  'margin-top': '40px',
		});
	    $('.view-commerce-cart-block .view-content .cart-items-wrapper').css('height', (Drupal.settings.dc_als_cart.itemlimit * Drupal.cart_item_height)+'px');
	    $('.view-commerce-cart-block .view-content .line-item-summary').css('margin-top', '40px');
	    break;
	}
    Drupal.setCarouselButtons(cart);
  };

  Drupal.unsetCarousel = function(cart) {
    cart.find('.cart-contents .cart-carousel-next').remove();
    cart.find('.cart-contents .cart-carousel-prev').remove();
	switch (Drupal.settings.dc_als_cart.direction) {
	  case "horizontal":
		$('.view-commerce-cart-block').css('width', '');
	    break;
	  case "vertical":
	    $('.view-commerce-cart-block .view-content').css({
		  'height': '',
		  'margin-top': '',
		});
	    $('.view-commerce-cart-block .view-content .cart-items-wrapper').css('height', '');
	    $('.view-commerce-cart-block .view-content .line-item-summary').css('margin-top', '');
	    break;
	}
    cart.find('.cart-contents').removeClass('cart-carousel-next-processed cart-carousel-prev-processed');
  };

  Drupal.moveProductToCart = function(ajax, callback) {
    //console.log(ajax.element);
    $('.'+ajax.form[0].attributes.id.value).parent().find('.messages').each(function() {
      $(this).parent().hide();
    });
//    var product = $(ajax.element).closest('.node-product-display').find('.field-name-field-printer-img');
//    if (!product.length)
      product = $(ajax.element).closest('.views-row').children('.views-field-field-product-image');
//    if (!product.length)
//      product = $('#field-slideshow-1-wrapper').children('.field-slideshow-1');
    var productX = product.offset().left;
		var productY = product.offset().top;
    var basket = $('#block-commerce-cart-cart');
    var basketX = basket.offset().left;
		var basketY = basket.offset().top;
    var gotoX = basketX - productX;
		var gotoY = basketY - productY;
    var newImageWidth 	= 170;
		var newImageHeight	= 69;
    product.clone()
      .prependTo('div.content-main')
      .css({
        'position' : 'absolute',
        'z-index' : '9999',
        'left' : productX+'px',
        'top' : productY+'px'
      })
      .animate({opacity: 0.7}, 100 )
      .animate({opacity: 0.2, marginLeft: gotoX, marginTop: gotoY, width: newImageWidth, height: newImageHeight}, 1200, function() {
        if (typeof callback === 'function') {
          callback.call();
        }
				$(this).remove();
		});
  };


})(jQuery,Drupal);
