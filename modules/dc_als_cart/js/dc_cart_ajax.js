(function($){
  var $wrapper = null;

  Drupal.ajax = Drupal.ajax || {};
  Drupal.ajax.prototype.commands.dc_als_cart = function(ajax, response, status) {
    /* $wrapper = $wrapper || $('#' + response['form-id']).parents('.view.view-commerce-cart-form:eq(0)').parent();*/
    $wrapper = $wrapper || $('[id^=' + response['form-id'] +']').parents('.view:eq(0)').parent() || $('#' + response['form-id']).parents('.view:eq(0)').parent();

    $wrapper
      .find('div.messages').remove().end()
      .prepend(response['message']);

    if (response.output != '') {
      $wrapper.find('#dc-als-cart-form-wrapper').html(response.output);
      return;
    }

    var fake_link = '#dc-als-cart-' + response['form-id'];
    $(fake_link).trigger('click');
  };

})(jQuery);