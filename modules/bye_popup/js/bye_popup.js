(function ($) {

//    $(document).ready(function () {
//
//    });

    /**
     * Example: inputPlaceholder( document.getElementById('my_input_element') )
     * @param {Element} input
     * @param {String} [color='#AAA']
     * @return {Element} input
     */
    function inputPlaceholder(input, color) {

        if (!input) return null;

        // Do nothing if placeholder supported by the browser (Webkit, Firefox 3.7)
        if (input.placeholder && 'placeholder' in document.createElement(input.tagName)) return input;

        color = color || '#AAA';
        var default_color = input.style.color;
        var placeholder = input.getAttribute('placeholder');

        if (input.value === '' || input.value == placeholder) {
            input.value = placeholder;
            input.style.color = color;
            input.setAttribute('data-placeholder-visible', 'true');
        }

        var add_event = /*@cc_on'attachEvent'||@*/'addEventListener';

        input[add_event](/*@cc_on'on'+@*/'focus', function(){
            input.style.color = default_color;
            if (input.getAttribute('data-placeholder-visible')) {
                input.setAttribute('data-placeholder-visible', '');
                input.value = '';
            }
        }, false);

        input[add_event](/*@cc_on'on'+@*/'blur', function(){
            if (input.value === '') {
                input.setAttribute('data-placeholder-visible', 'true');
                input.value = placeholder;
                input.style.color = color;
            } else {
                input.style.color = default_color;
                input.setAttribute('data-placeholder-visible', '');
            }
        }, false);

        input.form && input.form[add_event](/*@cc_on'on'+@*/'submit', function(){
            if (input.getAttribute('data-placeholder-visible')) {
                input.value = '';
            }
        }, false);

        return input;
    }

    function byePopup_setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function byePopup_getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    function modalOpen(dialog) {
        dialog.overlay.fadeIn('fast', function () {
            dialog.container.fadeIn('fast', function () {
                dialog.data.hide().slideDown('fast');
            });
        });
    }

    function simplemodal_close(dialog) {
        dialog.data.fadeOut('fast', function () {
            dialog.container.hide('fast', function () {
                dialog.overlay.slideUp('fast', function () {
                    $.modal.close();

                    byePopup_setCookie("bye_popup", 'SHOWN', 365);
                });
            });
        });
    }

    Drupal.behaviors.byePopup = {
        attach: function (context, settings) {

            $("#bye-popup input[type='text']").each(function(){
                inputPlaceholder($(this)[0]);
            });

            $(document).mousemove(function (e) {
                if (e.pageY <= window.scrollY + 5) {
                    var isCookie = byePopup_getCookie("bye_popup");
                    if(!isCookie) {
                        $('#bye-popup').modal({onOpen: modalOpen, onClose: simplemodal_close});
                    }
                }
            });
        }
    };

    $(document).ready(function(){
        if($("body.page-checkout-complete").length && !byePopup_getCookie("bye_popup")) {
            // Ставим куку, если был оформлен заказ
            byePopup_setCookie("bye_popup", 'ORDER', 365);
        }
    });

}(jQuery));
