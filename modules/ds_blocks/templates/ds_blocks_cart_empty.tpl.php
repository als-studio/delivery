<?php
?>

<div id="block-commerce-cart-cart-empty" class="  empty">
  <div class="cart-block-cart-image"></div>
  <div class="cart-block-title">Корзина</div>
  <div class="line-item-summary">
    <div class="line-item-summary-contents">
      <span class="line-item-quantity">0 товаров</span> на <div class="final-price">0 руб.</div>
    </div>
  </div>
  <div class="banner"></div>
  <div><a class="checkout-button disabled">Оформить заказ</a></div>
</div>