<?php

/**
 * Возвращает HTML-шаблон вывода блока копирайтов
 * 
 * @params $variables
 * Ассоциативный массив, содержащий:
 * - installing_year : год начала работы сайта
 * - current_year : текущий год
 * - trademark : зарегестрированный товарный знак
 */

?>

<p>
    <?php 
        $output = '©';

        if ( $variables['installing_year'] != null ) {
            $output .= $variables['trademark'];
            $output .= ', ';
        }

        if ( $variables['installing_year'] != null ) {
            $output .= $variables['installing_year'];
            if ( $variables['installing_year'] != $variables['current_year'] ) {
                $output .= '-';
                $output .= $variables['current_year'];
            }
        } else {
            $output .= $variables['current_year'];
        }

        $output .= '<br> Все права защищены';
        
        echo $output;
    ?>
</p>