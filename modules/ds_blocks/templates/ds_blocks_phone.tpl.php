<?php

/**
 * Возвращает HTML-шаблон вывода блока телефонного номера
 * 
 * @params $variables
 * Ассоциативный массив, содержащий:
 * - phone_prefix : префикс номера телефона (код города)
 * - phone : номер телефона
 */

?>

<p>
    <span><?php echo $variables['phone_prefix']; ?></span><?php echo $variables['phone']; ?>
</p>