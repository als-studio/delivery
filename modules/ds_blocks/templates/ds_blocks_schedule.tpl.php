<?php

/**
 * Возвращает HTML-шаблон вывода блока режима работы
 * 
 * @params $variables
 * Ассоциативный массив, содержащий:
 * - weekdays : первая группа (будние дни)
 * - weekends : вторая группа (выходные дни)
 */

?>

<p>
    <?php echo $variables['weekdays']; ?>
</p>
<p>
    <?php echo $variables['weekends']; ?>
</p>