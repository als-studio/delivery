<?php

/**
 * Возвращает HTML-шаблон вывода блока телефонного номера
 * 
 * @params $variables
 * Ассоциативный массив, содержащий:
 * - address : адрес заведения
 * - phone_prefix : префикс номера телефона (код города)
 * - phone : номер телефона
 */

?>

<p>
    Разработка: «<a href="/als-studio.ru" title="Сайт разработчика" target="_blank"><?php echo $variables['developer']; ?></a>»
</p>