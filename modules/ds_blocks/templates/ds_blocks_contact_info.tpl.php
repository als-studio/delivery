<?php

/**
 * Возвращает HTML-шаблон вывода блока контактной информации
 * 
 * @params $variables
 * Ассоциативный массив, содержащий:
 * - address : адрес заведения
 * - phone_prefix : префикс номера телефона (код города)
 * - phone : номер телефона
 */

?>

<p>
    Адрес: 
    <a href="/contact-info"><?php echo $variables['address']; ?></a>
</p>
<p>
    Телефон: <?php echo $variables['phone_prefix']; ?> <?php echo $variables['phone']; ?>
</p>