<?php

function ds_promotion_views_default_views() {
    $export = array();
    $view = new view();
    $view->name = 'special_offers';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = 'Специальные предложения и Акции';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Акции';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['use_more_text'] = 'ещё';
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['access']['perm'] = 'administer nodes';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
    $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
    $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
    $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
    $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Элементов на страницу';
    $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Все -';
    $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Пропустить';
    $handler->display->display_options['pager']['options']['tags']['first'] = '« первая';
    $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ предыдущая';
    $handler->display->display_options['pager']['options']['tags']['next'] = 'следующая ›';
    $handler->display->display_options['pager']['options']['tags']['last'] = 'последняя »';
    $handler->display->display_options['style_plugin'] = 'slideshow';
    $handler->display->display_options['style_options']['slideshow_type'] = 'views_slideshow_cycle';
    $handler->display->display_options['style_options']['slideshow_skin'] = 'default';
    $handler->display->display_options['style_options']['skin_info'] = array(
        'class' => 'default',
        'name' => 'По умолчанию',
        'module' => 'views_slideshow',
        'path' => '',
        'stylesheets' => array(),
    );
    $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['weight'] = '1';
    $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['type'] = 'views_slideshow_pager_fields';
    $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['views_slideshow_pager_fields_fields'] = array(
        'title' => 0,
        'field_photo' => 0,
        'body' => 0,
        'field_promo_end' => 0,
    );
    $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['weight'] = '1';
    $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['type'] = 'views_slideshow_controls_text';
    $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_slide_counter']['weight'] = '1';
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['weight'] = '1';
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['type'] = 'views_slideshow_pager_fields';
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['views_slideshow_pager_fields_fields'] = array(
        'title' => 0,
        'field_photo' => 0,
        'body' => 0,
        'field_promo_end' => 0,
    );
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['enable'] = 1;
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['weight'] = '1';
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['hide_on_single_slide'] = 1;
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['type'] = 'views_slideshow_controls_text';
    $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_slide_counter']['weight'] = '1';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['effect'] = 'slideX';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['timeout'] = '5000';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['speed'] = '700';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['delay'] = '0';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['start_paused'] = 0;
    $handler->display->display_options['style_options']['views_slideshow_cycle']['remember_slide_days'] = '1';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['items_per_slide'] = '1';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['wait_for_image_load_timeout'] = '3000';
    $handler->display->display_options['style_options']['views_slideshow_cycle']['cleartype'] = 0;
    $handler->display->display_options['style_options']['views_slideshow_cycle']['cleartypenobg'] = 0;
    $handler->display->display_options['row_plugin'] = 'fields';
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['label'] = '';
    $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
    $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
    $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
    $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
    /* Field: Content: Nid */
    $handler->display->display_options['fields']['nid']['id'] = 'nid';
    $handler->display->display_options['fields']['nid']['table'] = 'node';
    $handler->display->display_options['fields']['nid']['field'] = 'nid';
    $handler->display->display_options['fields']['nid']['node_in_colorbox_width'] = '600';
    $handler->display->display_options['fields']['nid']['node_in_colorbox_height'] = '600';
    $handler->display->display_options['fields']['nid']['node_in_colorbox_rel'] = '';
    /* Field: Draggableviews: Content */
    $handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
    $handler->display->display_options['fields']['draggableviews']['table'] = 'node';
    $handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
    $handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
    $handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
    $handler->display->display_options['fields']['draggableviews']['draggableviews']['hierarchy_handler'] = 'draggableviews_hierarchy_handler_native';
    $handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
    /* Sort criterion: Draggableviews: Weight */
    $handler->display->display_options['sorts']['weight']['id'] = 'weight';
    $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
    $handler->display->display_options['sorts']['weight']['field'] = 'weight';
    $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'special_offers:page_1';
    $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
    /* Filter criterion: Content: Type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = 'node';
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['value'] = array(
        'ds_promotion' => 'ds_promotion',
    );

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'fields';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['label'] = '';
    $handler->display->display_options['fields']['title']['exclude'] = TRUE;
    $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
    $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
    $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
    $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
    /* Field: Content: Фото */
    $handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
    $handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
    $handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
    $handler->display->display_options['fields']['field_photo']['label'] = '';
    $handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
    $handler->display->display_options['fields']['field_photo']['settings'] = array(
        'image_style' => 'slider_350_245',
        'image_link' => 'content',
    );
    $handler->display->display_options['path'] = 'promo';

    /* Display: Слайдер */
    $handler = $view->new_display('block', 'Слайдер', 'block_1');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['display_description'] = 'Блок в сайдбаре';
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Content: Фото */
    $handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
    $handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
    $handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
    $handler->display->display_options['fields']['field_photo']['label'] = '';
    $handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
    $handler->display->display_options['fields']['field_photo']['settings'] = array(
        'image_style' => 'slider_350_245',
        'image_link' => 'content',
    );

    /* Display: Сортировка акций */
    $handler = $view->new_display('page', 'Сортировка акций', 'page_1');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
        'title' => 'title',
        'field_photo' => 'field_photo',
        'body' => 'body',
        'field_promo_end' => 'field_promo_end',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
        'title' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'field_photo' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'body' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
        'field_promo_end' => array(
            'sortable' => 0,
            'default_sort_order' => 'asc',
            'align' => '',
            'separator' => '',
            'empty_column' => 0,
        ),
    );
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['path'] = 'promo/sort';
    $translatables['special_offers'] = array(
        t('Master'),
        t('Акции'),
        t('ещё'),
        t('Применить'),
        t('Сбросить'),
        t('Сортировать по'),
        t('По возрастанию'),
        t('По убыванию'),
        t('Элементов на страницу'),
        t('- Все -'),
        t('Пропустить'),
        t('« первая'),
        t('‹ предыдущая'),
        t('следующая ›'),
        t('последняя »'),
        t('Nid'),
        t('Content'),
        t('Page'),
        t('more'),
        t('Слайдер'),
        t('Блок в сайдбаре'),
        t('Сортировка акций'),
    );
    $export['test'] = $view;
    return $export;
}