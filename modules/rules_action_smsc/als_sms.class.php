<?php

require_once("smsc_api.php");

class ALSsmsc {
    private $login;
    private $password;
    
    public function __construct($smsclogin, $smscpass) {
        $this->login = $smsclogin;
        $this->password = $smscpass;
    }
    public function auth_check() {
        $balance = get_balance();
        if (!$balance || $balance <= 0) {
            return false;
        }
        return $balance;
    }
    public function sms_send($phones, $message, $translit) {
        $status = send_sms($phones, $message, $translit);
        if (!empty($status) && $status[1] > 0) {
            return $status;
        } else {
            return false;
        }
    }
}