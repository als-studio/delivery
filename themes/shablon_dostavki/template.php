<?php
/**
 * Move class from <a> to menu's <li>
 */
function shablon_dostavki_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  if (isset($element['#localized_options']['attributes']['id'])) {
    $element['#attributes']['id'] = $element['#localized_options']['attributes']['id'];    
  }

  unset($element['#localized_options']['attributes']['id']);

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
/**
 * Implements hook_form_alter().
 */
function shablon_dostavki_form_alter(&$form, $form_state, $form_id) {
 if (strpos($form_id, 'commerce_cart_add_to_cart_form_') === 0) {
            // Меняем стандартную надпись на кнопках оформления заказа в товаре
            $form['submit']['#attributes']['title'] = $form['submit']['#attributes']['value'] = "В корзину";
            // Добавляем +/- к полю количества
           // $form['submit']['#ajax']['progress'] = array('type' => 'throbber', 'message' => '');
 } 
 }
/**
 * Implements hook_form_FORM_ID_alter(): commerce_checkout_form_checkout.
 */
function shablon_dostavki_form_commerce_checkout_form_checkout_alter(&$form, &$form_state) {
  if (isset($form['account']['login']['mail'])) {
    $form['account']['login']['mail']['#required'] = FALSE;
  }
}
/*Убрать ресайз textarea*/
function shablon_dostavki_textarea($variables) {
  $element = $variables['element'];
  $element['#attributes']['name'] = $element['#name'];
  $element['#attributes']['id'] = $element['#id'];
  $element['#attributes']['cols'] = $element['#cols'];
  $element['#attributes']['rows'] = $element['#rows'];
  _form_set_class($element, array('form-textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    $wrapper_attributes['class'][] = 'resizable';
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}