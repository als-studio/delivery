<?php

function filamania_form_commerce_cart_add_to_cart_form_alter(&$form, &$form_state, $form_id) {
    $form['quantity']['#title'] = '';
    //dsm($form);
    $form['submit']['#weight'] = 100;
    if (isset($form['line_item_fields']['field_noodle_base'])) {
        foreach ($form['line_item_fields']['field_noodle_base']['und']['#options'] as $tid=>$name) {
            $term = taxonomy_term_load($tid);
            // dsm($term);
            // !! Ахтунг - костыль !!
            $price = round($term->field_price['und'][0]['amount']/100);
            //$price = commerce_currency_format($term->field_price['und'][0]['amount'], $term->field_price['und'][0]['currency_code']);
            $form['line_item_fields']['field_noodle_base']['und']['#options'][$tid] = $name . '<span class="price"><span class="price-amount">' . $price . '</span> руб.</span>';
        }      
    }    
    // Add price after argument title (into label)    
    if (isset($form['line_item_fields']['field_noodle_topping'])) {
        foreach ($form['line_item_fields']['field_noodle_topping']['und']['#options'] as $tid=>$name) {
            $term = taxonomy_term_load($tid);
            // dsm($term);
            // !! Ахтунг - костыль !!
            $price = round($term->field_price['und'][0]['amount']/100);
            //$price = commerce_currency_format($term->field_price['und'][0]['amount'], $term->field_price['und'][0]['currency_code']);
            $form['line_item_fields']['field_noodle_topping']['und']['#options'][$tid] = $name . '<span class="price"><span class="price-amount">' . $price . '</span> руб.</span>';
        }      
        $form['price'] = array(
            '#markup' => '<div id="fake-price">Итого цена: <span class="price"><span class="price-amount">110</span> руб.</span></div>',
            '#weight' => 40,
        );
    }
    
}

/**
 * Implements hook_views_pre_view().
 */
function filamania_views_pre_render($view) {
  if ($view->name == 'catalog') {
      $term = taxonomy_term_load($view->args[0]);
      $view->set_title($term->name);
  }
}

function filamania_form_commerce_checkout_form_checkout_alter(&$form, &$form_state, $form_id) {
    //echo "<pre>".print_r($form,1)."</pre>";
    // Перенос описания поля комментария в заголовок поля.
    $description = $form['customer_profile_billing']['field_comment'][LANGUAGE_NONE][0]['value']['#description'];
    $form['customer_profile_billing']['field_comment'][LANGUAGE_NONE][0]['value']['#title'] .= '<br/>'.$description;
    $form['customer_profile_billing']['field_comment'][LANGUAGE_NONE][0]['value']['#description'] = '';

    // Переименовываем кнопку
    $form['buttons']['continue']['#value'] = 'Заказать';
    // Убираем кнопку отмены
    unset($form['buttons']['cancel']);

}

function filamania_form_alter(&$form, &$form_state, $form_id) {
    if (isset($form['cart_contents'])){
        $form['cart_contents']['cart_contents_view']['#markup'] = str_replace('Order total', t('Order total'),$form['cart_contents']['cart_contents_view']['#markup']);
    }
}


/* Theme e-mail template for order */
function filamania_commerce_email_order_items($variables) {
  // Get order wrapper.
  $order_wrapper = $variables['commerce_order_wrapper'];
  // Get rendered view.
  return views_embed_view('commerce_cart_summary', 'default', $order_wrapper->getIdentifier());
}
