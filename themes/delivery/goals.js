(function ($) {
	Drupal.behaviors.goals = {
		attach: function (context, settings) {
			$('form.commerce-add-to-cart .form-submit').mousedown(function() {
				if (typeof yaCounter23034406 !== 'undefined') {
					/* 23034406 - ID счетчика, ADDTOCART - название цели */
					yaCounter23034406.reachGoal('ADDTOCART');
				}
			});
		}
	};
})(jQuery);