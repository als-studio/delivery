﻿	
(function ($) {
  Drupal.behaviors.catalog = {
    attach: function (context, settings) {
		
		$("div.view-catalog div.views-row").each(function(i) {
			if ($(this).find('form.commerce-add-to-cart input.form-radio').length > 0) {
				hideExtraLabels(this);
			}
			
		});
		
		/**
		 * Hide extra weight & price labels due to radios.
		 **/
		function hideExtraLabels(object) {
			/*$(object).find('div.commerce-product-field-field-weight').css('display','none');*/
			$(object).find('div.commerce-product-field-commerce-price').css('display','none');
		}
    }
  };
})(jQuery);
