<div class="content-main"><!-- Общий контейнер. -->
	<!-- Шапка - начало. -->
	<div class="head">
		
		<div class="head-top">
			<div class="head-box">
				<div class="site_info">
				<?php if ($logo): ?>
			    	<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
			        	<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
			      	</a>
			    <?php endif; ?>	
					<p class="site_slogan">
						<?php print $site_slogan; ?>
					</p>
				</div><!--/site_info-->
				<?php print render($page['header_top']); ?>
			</div><!-- /head-box -->
		</div><!-- /head-top -->
		<div class="head-slider">
			<?php print render($page['header_slider']); ?>
		</div>
			<div class="head-bottom">
		<div class="head-box">
		
				<?php print render($page['header_bottom']); ?>					
				<div class="clear"></div>
				
			
		</div><!-- /head-box -->
		</div><!--head-bottom-->
		<div class="page-title">
			
					<div class="head-box"><?php print render($title_prefix); ?>
                    <?php if ($title): ?>
			        <h1><?php print $title ?></h1>
			        <?php endif; ?>
                <?php print render($title_suffix); ?>
				</div>
				
		</div>
			</div><!-- /head -->
	<!-- Шапка - конец. -->


	<!-- Основной контент - начало. -->
	<div class="content-wrapper">
		<div class="content-box">		  
			<?php if ($tabs): ?>
			<div class="tabs">
				<?php print render($tabs); ?>
			</div><!-- /tabs -->
			<?php endif; ?>
			<div class="content">
				<!--<div class="bread_box"><?php print $breadcrumb; ?></div>-->
                <?php print $messages; ?>
				<?php print render($page['content']); ?>
			</div><!--/column-->
			
			<?php if ($page['sidebar_first']): ?>
			<div class="sidebar">
			<?php print render($page['sidebar_first']); ?>
			</div><!--/sidebar-->
			<?php endif; ?>
			<div class="clear"></div>
		</div><!-- /content-box -->
	</div><!-- /content-wrapper -->
	<!-- Основной контент - конец. -->
	<div class="wrapper-footer_empty">&nbsp;</div><!-- Прижимаем подвал вниз. -->

	<!-- /Подвал - начало. -->
	
	<div id="footer">
		<div class="footer-box">
			<div class="footer-inside">
				<div class="footer-logo"></div>
				<?php print render($page['footer']); ?>
			</div><!-- /footer-inside -->
		</div><!-- /footer-box -->
	</div><!-- /#footer -->
	<!-- /Подвал  конец. -->
</div><!-- /content -->	
