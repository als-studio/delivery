(function ($) {
    function wok_updateTotal() {
        var price = 0;

        $('body.node-type-noodle .node-noodle form input:checked').each(function () {
            var $price = $('.commerce-add-to-cart label[for="' + $(this).attr('id') + '"] .price-amount');
            if($price.length) {
                var pricePlus = parseInt($price.text());
                price += pricePlus ? pricePlus : 0;
            }
        });

        $("#fake-price .price-amount").text(price);
    }

    Drupal.behaviors.wok = {
        attach: function (context, settings) {
            if ($('body.node-type-noodle').length <= 0)
                return;
            $('body.node-type-noodle .node-noodle form input').change(wok_updateTotal);
            wok_updateTotal();
        }
    };
})(jQuery);
