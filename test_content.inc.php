<?php

/**
 * @file
 */

// Машинные имена словарей таксономии: _delivery_get_test_vocabularies() и _delivery_get_test_terms()
define('VOCABULARY_CATALOG', 'catalog');

// Машинные имена меню: _delivery_get_test_menus()
define('MENU_CATALOG_MENU', 'catalog-menu');

// Пути: _delivery_get_test_menu_links()
define('ALIAS_SUSHI', 'sushi');
define('ALIAS_SOUPS', 'soups');
define('ALIAS_SALADS_AND_SNACKS', 'salads-and-snacks');
define('ALIAS_DESSERTS_AND_DRINKS', 'desserts-and-drinks');
define('ALIAS_WOK_AND_BENTO', 'wok-and-bento');
define('ALIAS_SETS_OF_ROLLS', 'sets-of-rolls');
define('ALIAS_PIZZA_AND_PIES', 'pizza-and-pies');

// Машинные имена полей: _delivery_get_test_fields() и _delivery_get_instances()
define('FIELD_FOOD_TYPE', 'field_food_type');

/**
 * Создание тестового контента
 */
function delivery_test_content_install()
{
    foreach (_delivery_get_test_vocabularies() as $vocabulary_name => $vocabulary) {
        taxonomy_vocabulary_save((object)$vocabulary);
    }

    foreach (_delivery_get_test_terms() as $vocabulary_machine_name => $term_item) {

        $vocabulary_vid = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name)->vid;

        foreach ($term_item as $item) {
            $term = (object)array(
                'name' => $item['name'],
                'vid'  => $vocabulary_vid,
            );
            $term->path = array(
                'alias'    => $item['alias'],
                'pathauto' => false,
                'language' => 'und',
            );
            taxonomy_term_save($term);
        }
    }

    foreach (_delivery_get_test_menus() as $menu_name => $menu) {
        menu_save($menu);
    }

    foreach (_delivery_get_test_menu_links() as $alias_name => $link) {
        menu_link_save($link);
    }

    menu_rebuild();

    $query = db_insert('block')->fields(
        array(
            'module',
            'delta',
            'title',
            'theme',
            'status',
            'weight',
            'region',
            'pages',
            'cache',
        )
    );

    foreach (_delivery_get_test_blocks() as $block) {
        $query->values($block);
    }

    $query->execute();

    foreach (_delivery_get_test_fields() as $field_name => $field) {
        field_create_field($field);
    }

    foreach (_delivery_get_test_instances() as $instance_name => $instance) {
        field_create_instance($instance);
    }

    // Создание тестовых товаров все еще нуждается в оптимизации
    $product = commerce_product_new('product');
    $product->uid = 1;
    $product->sku = 'cheesy_balls';
    $product->title = 'Сырные шарики';
    $product->language = LANGUAGE_NONE;
    $product->commerce_price[LANGUAGE_NONE][0]['amount'] = 249 * 100;
    $product->commerce_price[LANGUAGE_NONE][0]['currency_code'] = 'RUB';
    $product->field_show_full_title[LANGUAGE_NONE][0]['value'] = TRUE;
    $product->field_consist[LANGUAGE_NONE][0]['value'] = 'Описание сырных шариков, обжаренных в сухарях.';
    $product->field_consist[LANGUAGE_NONE][0]['value'] = 'Cырные шарики, обжаренные в сухарях.';
    $product->field_show_weight[LANGUAGE_NONE][0]['value'] = TRUE;
    $product->field_food_type[LANGUAGE_NONE][0]['value'] = 3;
    commerce_product_save($product);

    $node = (object)array(
        'type' => 'product_display',
    );
    node_object_prepare($node);
    $node->title = "Сырные шарики";
    $node->uid = 1;
    $node->field_product[LANGUAGE_NONE][]['product_id'] = $product->product_id;
    $node->language = LANGUAGE_NONE;
    node_save($node);

    $product = commerce_product_new('product');
    $product->uid = 2;
    $product->sku = 'dragon';
    $product->title = 'Дракон';
    $product->language = LANGUAGE_NONE;
    $product->commerce_price[LANGUAGE_NONE][0]['amount'] = 559 * 100;
    $product->commerce_price[LANGUAGE_NONE][0]['currency_code'] = 'RUB';
    $product->field_show_full_title[LANGUAGE_NONE][0]['value'] = TRUE;
    $product->field_consist[LANGUAGE_NONE][0]['value'] = 'Описание пиццы Дракон.';
    $product->field_consist[LANGUAGE_NONE][0]['value'] = 'Ароматные колбаски пепперони, пицца-соус, шампиньоны и ветчина, а также сыры - Моцарелла и Пармезан! Знаменита своей остротой, которая достигается благодаря жгучему перчику халапенью.';
    $product->field_show_weight[LANGUAGE_NONE][0]['value'] = TRUE;
    $product->field_food_type[LANGUAGE_NONE][0]['value'] = 7;
    commerce_product_save($product);

    $node = (object)array(
        'type' => 'product_display',
    );
    node_object_prepare($node);
    $node->title = "Дракон";
    $node->uid = 1;
    $node->field_product[LANGUAGE_NONE][]['product_id'] = $product->product_id;
    $node->language = LANGUAGE_NONE;
    node_save($node);
}

/**
 * Функция возвращает словарей, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_vocabularies()
{
    return array(
        VOCABULARY_CATALOG => array(
            'name'         => 'Разделы меню',
            'machine_name' => VOCABULARY_CATALOG,
            'description'  => 'Этот словарь используется для построения меню каталога.',
        ),
    );
}

/**
 * Функция возвращает словарей, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_terms()
{
    return array(
        VOCABULARY_CATALOG => array(
            array(
                'name'  => 'Суши, роллы',
                'alias' => 'sushi',
            ),
            array(
                'name'  => 'Супы, горячее',
                'alias' => 'soups',
            ),
            array(
                'name'  => 'Салаты, закуски',
                'alias' => 'salads-and-snacks',
            ),
            array(
                'name'  => 'Десерты, напитки',
                'alias' => 'desserts-and-drinks',
            ),
            array(
                'name'  => 'WOK, бенто-ланч',
                'alias' => 'wok-and-bento',
            ),
            array(
                'name'  => 'Наборы роллов',
                'alias' => 'sets-of-rolls',
            ),
            array(
                'name'  => 'Пицца, пироги',
                'alias' => 'pizza-and-pies',
            ),
        ),
    );
}


/**
 * Функция возвращает словарей, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_menus()
{
    return array(
        MENU_CATALOG_MENU => array(
            'menu_name'   => 'catalog-menu',
            'title'       => st('Catalog menu'),
            'description' => 'Меню для каталога',
        ),
    );
}

/**
 * Функция возвращает структурированный массив ссылок меню, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_menu_links()
{
    return array(
        ALIAS_SUSHI               => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'Суши, роллы',
            'link_path'  => drupal_lookup_path('source', ALIAS_SUSHI),
        ),
        ALIAS_SOUPS               => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'Супы, горячее',
            'link_path'  => drupal_lookup_path('source', ALIAS_SOUPS),
        ),
        ALIAS_SALADS_AND_SNACKS   => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'Салаты, закуски',
            'link_path'  => drupal_lookup_path('source', ALIAS_SALADS_AND_SNACKS),
        ),
        ALIAS_DESSERTS_AND_DRINKS => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'Десерты, напитки',
            'link_path'  => drupal_lookup_path('source', ALIAS_DESSERTS_AND_DRINKS),
        ),
        ALIAS_WOK_AND_BENTO       => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'WOK, бенто-ланч',
            'link_path'  => drupal_lookup_path('source', ALIAS_WOK_AND_BENTO),
        ),
        ALIAS_SETS_OF_ROLLS       => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'Наборы роллов',
            'link_path'  => drupal_lookup_path('source', ALIAS_SETS_OF_ROLLS),
        ),
        ALIAS_PIZZA_AND_PIES      => array(
            'menu_name'  => MENU_CATALOG_MENU,
            'link_title' => 'Пицца, пироги',
            'link_path'  => drupal_lookup_path('source', ALIAS_PIZZA_AND_PIES),
        ),
    );
}

/**
 * Функция возвращает структурированный массив блоков, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_blocks()
{
    return array(
        array(
            'module' => 'menu',
            'delta'  => MENU_CATALOG_MENU,
            'title'  => '<none>',
            'theme'  => THEME_DELIVERY,
            'status' => 1,
            'weight' => -15,
            'region' => 'header_bottom',
            'pages'  => '',
            'cache'  => -1,
        ),
    );
}

/**
 * Функция возвращает структурированный массив полей, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_fields()
{
    return array(
        FIELD_FOOD_TYPE => array(
            'field_name'  => FIELD_FOOD_TYPE,
            'type'        => 'taxonomy_term_reference',
            'cardinality' => FIELD_CARDINALITY_UNLIMITED,
            'settings'    => array(
                'allowed_values' => array(
                    array(
                        'vid'        => taxonomy_vocabulary_machine_name_load(VOCABULARY_CATALOG)->vid,
                        'vocabulary' => 'catalog',
                        'parent'     => 0,
                    ),
                ),
            ),
        ),
    );
}

/**
 * Функция возвращает структурированный массив экземпляров полей, определенных в этом профиле.
 * @return array
 */
function _delivery_get_test_instances()
{
    return array(
        FIELD_FOOD_TYPE => array(
            'field_name'  => FIELD_FOOD_TYPE,
            'entity_type' => 'commerce_product',
            'label'       => 'Раздел меню',
            'bundle'      => 'product',
            'required'    => true,
            'widget'      => array(
                'type' => 'taxonomy_autocomplete',
            ),
            'display'     => array(
                'default'   => array(
                    'type' => 'hidden',
                ),
                'line_item' => array(
                    'type' => 'hidden',
                ),
                'teaser'    => array(
                    'type' => 'hidden',
                ),
            ),
        ),
    );
}
